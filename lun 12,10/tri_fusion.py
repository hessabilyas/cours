from math import exp
def interclassement(l1, l2) -> list:
    LN = []
    n1, n2 = len(l1), len(l2)
    i1, i2 = 0,0
    while i1<n1 and i2<n2:
        if l1[i1] < l2[i2]:
            LN.append(l1[i1])
            i1 += 1
        else:
            LN.append(l2[i2])
            i2 += 1
    return LN + l1[i1:] + l2[i2:]

def fusion(L):
    if len(L) <= 1:
        return L
    else:
        m = len(L) // 2
        return interclassement(fusion(L[:m]), fusion(L[m:]))

ls = [5,4,9,2,7,5,4,1]

print(fusion(ls))

def fonction(x):
    return(x * exp(x))

def dichotomie(a,b,c):
    if b-a < 0.1:
        return a 
    while b - a > 0.1:
        m = (a + b) /2
        if fonction(m) < c:
            return (dichotomie(m, b, c))
        else:
            return(dichotomie(a, m, c))

z = -1
o = 3
i = 60
print(dichotomie(z, o, i))


def tri_rapide(t):
    pivot = t[0]
    t1 = []
    t2 = []
    for k in t[1:]:
        if max(k, pivot) == pivot:
            k.append(t1)
        else:
            k.append(t2)
    
