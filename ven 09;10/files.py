class file:
    """
    Implémentation du type abstrait file avec des objets de type list
    """
    def __init__(self):
        self._file = []

    def est_vide(self):
        return self._file == []

    def emfiler(self, val):
        ls = []
        ls.append(val)
        self._file = ls + self._file

    def defiler(self):
        #sommet = self._file[-1]
        #self._file = self._file(:-1)
        #return sommet
        if self.est_vide():
            raise IndexError("La liste est vide elle ne peut être défiler")
        return self._file.pop(1)

    def __repr__(self):
        if self.est_vide():
            return "|-"
        else:
            return f"{self.file[-1]}-|"

