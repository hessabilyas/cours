import string
import itertools

def decale(lettre, dec):
    alph = string.ascii_uppercase
    new_index = (alph.index(lettre) + dec) % 26
    return alph[new_index]

def cesar(message, dec):
    res = []
    for l in message:res.append(decale(l, dec))
    return "".join(res)

def vigenere(message, passwd):
    res = []
    for lettre, decl in zip(message,itertools.cycle(passwd)):
        dec = string.ascii_uppercase.index(decl)
        res.append(decale(lettre , dec))
    return "".join(res)

print(help(string))