import random

def scalaire(v1: tuple, v2: tuple) -> float:
    """ Calcule le produite scalaire des deux
        vecteurs v1 et v2
    """
    s = 0
    print(f"-->{v1},{v2}")
    for k in range(len(v1)):
        s = s + v1[k] * v2[k]
    return s

def random_vect(n: int) -> tuple:
    """ Choisit un vecteur de R^n contenant des entiers
        non nuls entre -10 et 10
    """
    v = []
    while len(v)<3:
        val = random.randint(-10, 10)
        if val != 0:
            v.append(val)
    return tuple(v)

def main():
    for i in range(20):
        print(scalaire(random_vect(3), random_vect(3)))



print(main())