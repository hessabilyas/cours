import math
def terme(k: int) -> float:
    return 1 / (k ** 2)

def approxpi(n: int) -> float:
    s = 0
    for k in range(n+1):
        s = s + terme(k)
    return math.sqrt(s * 6)


entrée = 1000

fonction = approxpi(entrée)

print(fonction)