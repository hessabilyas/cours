
rsx_social = {
    "Ana": ("Bob"),
    "Carl": (""),
    "Bob": ("Ana", "Carl", "Erol"),
    "Erol": ("Bob", "Gab"),
    "Dora": ("Erol"),
    "Gab": ("Erol", "Filo"),
    "Filo": ("Erol")
}

def amis_d_amis(dico: dict, pers: str) -> list:
    amis_d_amis = []
    for ami in dico[pers]:
        for a in dico[ami]:
            if a != pers and a not in amis_d_amis:
                amis_d_amis.append(a)
    return amis_d_amis

print(amis_d_amis(rsx_social, "Bob"))