def mlrusse(x:int, y:int, acc:int) -> int:
    if x == 0:
        return acc
    else:
        if x%2 == 0:
            return mlrusse(x/2, y*2, acc)
        else:
            return mlrusse((x-1)/2, y*2, acc + y)

print(mlrusse(200, 3, 0))

def mlrusse2(x: int, y: int, acc: int = 0) -> int:
    if x == 0:
        return acc
    else:
        return mlrusse2(x//2, y*2, acc + y * (x%2))

def mlrusseimp(x: int, y: int, acc: int = 0) -> int:
    acc = 0
    while x != 0:
        acc += y * (x%2)
        x //= 2
        y *= 2
    return acc