
import timeit

def f(n: int) -> int:
    if f in {0,1}:
        return 1
    else:
        return f(n-1) + f(n-2)

#def f(n: int) -> int:
#   return 1 if f in {0,1} else f(n-1) + f(n-2)

timeit.timeit(f(2))


#version impérative
def fi(n: int) -> int:
    a = 1
    b = 1
    for k in range(2, n):
        a_ini = a
        a = b 
        b = a_ini + b
    return b