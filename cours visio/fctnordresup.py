#f: T -> T
#x: T
#truc: T -> T
#myst : (T -> T) -> (T -> T)
#Avec typing, T -> T s'écrit Callable[[T], T]
from typing import Callable, TypeVar

T = TypeVar('T')


def myst(f: Callable[[T], T]) -> :
    def truc(x: T) -> T:
        return f(f(x))
    return truc

def g(c: str) -> str:
    return chr(ord(c) + 3)

h: Callable[[str], str]= myst(g)



def derive(f):
    def approx(a):
        def presicion(h):
            return (f(a + h) - f(a)) / h
        return precision
    return approx
