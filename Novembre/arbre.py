class Arbre():
    def __init__(self, parent):
        self.gauche = None
        self.parent = parent
        self.droite = None

    def get_gauche(self):
        return self.gauche
    
    def get_droite(self):
        return self.droite

    def set_gauche(self, val):
        self.gauche = val

    def set_droite(self,val):
        self.droite = val

    def insertion(self, valeur):
        if valeur > parent:
            self.set_droite(valeur)
        else:
            self.set_gauche(valeur)