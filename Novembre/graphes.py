class Graphe:
    def __init__(self, mat):
        self.m = mat
    
    def est_lié(self, i, j):
        return self.m[i][j] == 1

    def est_symétrique(self):
        for i in range(len(self.m)):
            for j in range(len(self.m[0])):
                if self.m[i][j] != self[i][j]:
                    return False
        return True
