#a. Créer la fonction applique(fonction, liste) qui remplit ce rôle.
def applique(f, xs):
    return[f(x) for x in xs]

#liste = ['a', 'b', 'c', 'd']

#b. Utiliser applique  pour mettre  les éléments  d'une liste  de chaînes  de caractères en majuscule. 
# On pourra utiliser la méthode upper.
#print(applique(str.upper, liste))

#c. Que désignera la variable mystere définie par :
mystere = lambda liste: applique(lambda nb: nb**2, liste)
ls = [0, 2, 3]
#print(applique(mystere, ls))
#la variable désignera le carré des nombres de la liste

def reduire(fonction, depart, xs):
    print(depart)
    for x in xs:
        depart = fonction(depart,x)
    return depart

print( reduire(lambda x, y: x + y , '', ['a', 'b', 'c', 'd']))


