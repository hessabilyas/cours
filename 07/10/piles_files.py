from Structures.ipynb import Maillon

class Pile:
    """
    Implémentation du type abstrait pile avec les maillons
    """
    def __init__(self: "Pile") -> None:
        self._sommet = None 

    def est_vide(self: "Pile") -> bool:
        return self._sommet is None

    def empiler(self: "Pile", val: T) -> None:
        s: Maillon = Maillon(val)
        if self._sommet is None:
            sommet = Maillon(val)
        else:
            s.set_suiv(self._sommet)
            self._sommet = s

    def depiler(self):
        if self._sommet is None:
            raise IndexError("La pile est vide, elle ne peut être dépilée")
        else:
            s: T = self._sommet.get.val()
            self._sommet = self._sommet.get_suiv()
            return(s)
        

    def __repr__(self):
        if self.est_vide():
            return "|-"
        else:
            return f"{self.sommet}-|"
