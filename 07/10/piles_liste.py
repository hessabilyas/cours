class Pile:
    """
    Implémentation du type abstrait pile avec des objets de type list
    """
    def __init__(self):
        self._pile = []

    def est_vide(self):
        return self._pile == []

    def empiler(self, val):
        self._pile.append(val)

    def depiler(self):
        #sommet = self._pile[-1]
        #self._pile = self._pile(:-1)
        #return sommet
        if self.est_vide():
            raise IndexError("La liste est vide elle ne peut être dépiler")
        return self._pile.pop(-1)

    def __repr__(self):
        if self.est_vide():
            return "|-"
        else:
            return f"{self.pile[-1]}-|"
