ar = (((None, "O", None), "L", ((None, "H", None),"R", None)),"A",((None, "M", None), "I", (None, "E", None)),"G",(None, "T", None))
ar2 = (None, "D", None)

def trouve(val, arbre: tuple) -> bool:
    if arbre[0] is None and arbre[2] is None:
        return True if val == arbre[1] else False
    elif arbre[0] is None and arbre[2] is not None:
        return trouve(val, arbre[2])
    elif arbre[0] is not None and arbre[2] is None:
        return trouve(val, arbre[0])
    else:
        return trouve(val, arbre[0]) or trouve(val,arbre[2])

print(trouve("E", ar))

    