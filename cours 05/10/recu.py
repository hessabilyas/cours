def somme(n):
    if n == 0:
        return 0
    else:
        return(n + somme(n-1))

def somme_carré(n):
    if n**2 == 0:
        return 0
    else:
        return(n**2 + somme(n-1))

def somme1(n):
    if n == 0:
        return 2
    else:
        return(4 * somme1(n-1) + 5)

def fibonacci(a, b, n):
    if n == 0:
        return a
    elif n == 1:
        return b
    else:
        return(fibonacci(a, b, (n-1)) + fibonacci(a, b, (n-2)))

def PGCD(a,b):
    while b != 0:
        a, b = b, a%b
    return a

def PGCD_recu(a,b):
    if b == 0:
        return a
    else:
        return(PGCD(b,a%b))

def expo(a,n):
    if n == 0:
        return 1
    elif n%2 == 0:
        
    else: 
        return(a * expo(a,n-1))
        